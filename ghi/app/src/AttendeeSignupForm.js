import React from 'react';

class AttendeeSignupForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            conference: '',
            conferences: [],
        }
    }
    
    async componentDidMount() {
        const resp = await fetch('http://localhost:8000/api/conferences/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                conferences: data.conferences
            });

            const selectTag = document.getElementById('conference');    
            const loadingIcon = document.getElementById('loading-conference-spinner');
            loadingIcon.classList.add("d-none");
            selectTag.classList.remove('d-none');
        }
    }

    handleConferenceChange = event => {
        this.setState({
            conference: event.target.value
        })
    }

    handleNameChange = event => {
        this.setState({
            name: event.target.value
        })
    }

    handleEmailChange = event => {
        this.setState({
            email: event.target.value
        })
    }

    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences;
        const resp = await fetch('http://localhost:8001/api/attendees/', {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newAttendee = await resp.json();
            const cleared = {
                conference: '',
                name: '',
                email: '',
            };
            this.setState(cleared);
        }
    }

    render() {
        let spinnerClasses = 'd-flex justify-content-center mb-3';
        let dropdownClasses = 'form-select d-none';
        if (this.state.conferences.length > 0) {
            spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
            dropdownClasses = 'form-select';
        }

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (this.state.hasSignedUp) {
            messageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none';
        }

        return (
            <div className="my-5 container">
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={this.handleSubmit} id="create-attendee-form">
                                    <h1 className="card-title">It's Conference Time!</h1>
                                    <p className="mb-3">
                                    Please choose which conference
                                    you'd like to attend.
                                    </p>
                                    <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                        <div className="spinner-grow text-secondary" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <div className="mb-3">
                                        <select onChange={this.handleConferenceChange} value={this.state.conference} name="conference" id="conference" className="form-select d-none" required>
                                            <option value="">Choose a conference</option>
                                            {
                                                this.state.conferences.map(conference => {
                                                    return <option key={conference.href} value={conference.href}>{conference.name}</option>
                                                })
                                            }
                                        </select>
                                    </div>
                                    <p className="mb-3">
                                        Now, tell us about yourself.
                                    </p>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleNameChange} value={this.state.name} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                                                <label htmlFor="name">Your full name</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={this.handleEmailChange} value={this.state.email} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                                                <label htmlFor="email">Your email address</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">I'm going!</button>
                                </form>
                                <div className="alert alert-success d-none mb-0" id="success-message">
                                    Congratulations! You're all signed up!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    } 
}

export default AttendeeSignupForm;