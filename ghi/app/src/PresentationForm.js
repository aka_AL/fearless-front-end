import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title: '',
            synopsis: '',
            conference: '',
            conferences: []
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({
                conferences: data.conferences
            });
        }
    }

    handlePresenterNameChange = event => {
        this.setState({
            presenterName: event.target.value
        })
    }

    handlePresenterEmailChange = event => {
        this.setState({
            presenterEmail: event.target.value
        })
    }

    handleCompanyNameChange = event => {
        this.setState({
            companyName: event.target.value
        })
    }

    handleTitleChange = event => {
        this.setState({
            title: event.target.value
        })
    }

    handleSynopsisChange = event => {
        this.setState({
            synopsis: event.target.value
        })
    }

    handleConferenceChange = event => {
        this.setState({
            conference: event.target.value
        })
    }

    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.presenterName;
        data.presenter_email = data.presenterEmail;
        data.company_name = data.companyName;
        delete data.presenterName;
        delete data.presenterEmail;
        delete data.companyName;
        delete data.conferences;

        const conferenceId = data.conference;
        const resp = await fetch(`http://localhost:8000/api/conferences/${conferenceId}/presentations/`, {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newPresentation = await resp.json();
            const cleared = {
                presenterName: '',
                presenterEmail: '',
                companyName: '',
                title: '',
                synopsis: '',
                conference: '',
            };
            this.setState(cleared);
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.presenterName} onChange={this.handlePresenterNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.presenterEmail} onChange={this.handlePresenterEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.companyName} onChange={this.handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.title} onChange={this.handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.synopsis} onChange={this.handleSynopsisChange} placeholder="Synopsis" required type="textarea" name="synopsis" id="synopsis" className="form-control"/>
                                <label htmlFor="synopsis">Synopsis</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.conference} onChange={this.handleConferenceChange} required name="conference" id="conference" className="form-select">
                                    <option value="default">Choose a conference</option>
                                    {
                                        this.state.conferences.map(conference => {
                                            return <option key={conference.id} value={conference.id}>{conference.name}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default PresentationForm;