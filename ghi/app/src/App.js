import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendeeSignupForm from './AttendeeSignupForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App(props) {
    if (props.attendees === undefined) {
        return null;
    }
    return (
        <React.Fragment>
            <BrowserRouter>
                <Nav />
                <Routes>
                    <Route index element={<MainPage />} />
                    <Route path="/conferences/new" element={<ConferenceForm />}/>
                    <Route path="/attendees/new" element={<AttendeeSignupForm />}/>
                    <Route path="/locations/new" element={<LocationForm />}/>
                    <Route path="/presentations/new" element={<PresentationForm />}/>
                    <Route path="/attendees" element={<AttendeesList attendees={props.attendees}/>}/>
                </Routes>
            </BrowserRouter>
            <div className="container">
                {/* <AttendeeSignupForm />
                <ConferenceForm />
                <LocationForm />
                <AttendeesList attendees={props.attendees}/> */}
            </div>
        </React.Fragment>
    );
}

export default App;
