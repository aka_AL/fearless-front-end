import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            locations: []
        };
    }
    
    async componentDidMount() {
        const resp = await fetch('http://localhost:8000/api/locations/');
        const data = await resp.json();
        this.setState({
            locations: data.locations
        });
    }

    handleNameChange = event => {
        this.setState({
            name: event.target.value
        })
    }

    handleStartChange = event => {
        this.setState({
            starts: event.target.value
        })
    }

    handleEndChange = event => {
        this.setState({
            ends: event.target.value
        })
    }

    handleDescriptionChange = event => {
        this.setState({
            description: event.target.value
        })
    }

    handleMaxPresentationsChange = event => {
        this.setState({
            maxPresentations: event.target.value
        })
    }

    handleMaxAttendeesChange = event => {
        this.setState({
            maxAttendees: event.target.value
        })
    }

    handleLocationChange = event => {
        this.setState({
            location: event.target.value
        })
    }

    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;

        const resp = await fetch('http://localhost:8000/api/conferences/', {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newConference = await resp.json();
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: '',
            };
            this.setState(cleared);
        }
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={this.handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleStartChange} value={this.state.starts} placeholder="Start date" required type="date" name="starts" id="starts" className="form-control"/>
                        <label htmlFor="start_date">Start date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleEndChange} value={this.state.ends} placeholder="End date" required type="date" name="ends" id="ends" className="form-control"/>
                        <label htmlFor="end_date">End date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleDescriptionChange} value={this.state.description} placeholder="Description" required type="textarea" name="description" id="description" className="form-control"/>
                        <label htmlFor="description">Description</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxPresentationsChange} value={this.state.maxPresentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                        <label htmlFor="max_presentations">Max presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxAttendeesChange} value={this.state.maxAttendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                        <label htmlFor="max_attendees">Max attendees</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                            <option value="">Choose a location</option>
                            {
                                this.state.locations.map(location => {
                                    return <option key={location.id} value={location.id}>{location.name}</option>
                                })
                            }
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        );
    }
}
  
export default ConferenceForm;