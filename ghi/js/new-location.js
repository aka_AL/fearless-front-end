window.addEventListener('DOMContentLoaded', async () => {
    const stateUrl = "http://localhost:8000/api/states/";
    const stateResponse = await fetch(stateUrl);
    if (!stateResponse.ok) {
        console.log("Sorry, looks like something went wrong.");
    } else {
        const data = await stateResponse.json();
        const stateSelect = document.getElementById("state");
        data.states.forEach(state => {
            stateSelect.innerHTML += `<option value="${state.abbreviation}">${state.name}</option>`;
        });
    }

    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    });
});