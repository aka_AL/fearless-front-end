window.addEventListener('DOMContentLoaded', async () => {
    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const conferenceResponse = await fetch(conferenceUrl);
    if (!conferenceResponse.ok) {
        console.log("Sorry, looks like something went wrong.");
    } else {
        const data = await conferenceResponse.json();
        const conferenceSelect = document.getElementById("conference");

        data.conferences.forEach((conference) => {
            conferenceSelect.innerHTML += `<option value="${conference.id}">${conference.name}</option>`;
        });
    }


    const formTag = document.getElementById("create-presentation-form");
    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const select = document.getElementById('conference');
        const selectedIndex = select.selectedIndex;
        const selectedValue = select.options[selectedIndex].value;
        const presentationUrl = `http://localhost:8000/api/conferences/${selectedValue}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
        }
    });
});