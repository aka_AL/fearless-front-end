function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
        <div class="col-4">
            <div class="card shadow p-3 m-2 bg-body rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    <p class="card-text">${starts} - ${ends}</p>
                </div>
            </div>
        </div>
    `;
}


export function errorMessage () {
    const row = document.querySelector('.row');
    row.innerHTML = `<div class="alert alert-warning" role="alert">
    Sorry, looks like something went wrong.
    </div>`;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            errorMessage();
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = new Date (details.conference.starts);
                    const startsFormatted = starts.toLocaleDateString();
                    const ends = new Date (details.conference.ends);
                    const endsFormatted = ends.toLocaleDateString();
                    const html = createCard(name, description, pictureUrl, location, startsFormatted, endsFormatted);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }    
            }
        }
    } catch (e) {
        errorMessage();
    }
});