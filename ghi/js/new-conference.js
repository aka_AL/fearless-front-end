window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = "http://localhost:8000/api/locations/";
    const locationResponse = await fetch(locationUrl);
    if (!locationResponse.ok) {
        console.log("Sorry, looks like something went wrong.");
    } else {
        const data = await locationResponse.json();
        const locationSelect = document.getElementById("location");
        data.locations.forEach(location => {
            locationSelect.innerHTML += `<option value="${location.id}">${location.name}</option>`;
        });
    }

    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
        }
    });
});